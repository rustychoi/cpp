#!/usr/bin/env bash

( 
    set -x
    time cmake 'CMakeLists.txt'
    echo 
    time make -j --print-directory
)
