#include <iostream>
#include <vector>

// pass-by-value
void double_v1(std::vector<int> toDouble) {
    //
    std::cout << "double_v1, before for loop:" << std::endl;
    for (int i = 0; i < toDouble.size(); i++) {
        std::cout << "  x = " << toDouble[i] << std::endl;
    }

    //
    for (int i = 0; i < toDouble.size(); i++) {
        toDouble[i] *= 2;
    }

    //
    std::cout << "double_v1, after for loop:" << std::endl;
    for (int i = 0; i < toDouble.size(); i++) {
        std::cout << "  x = " << toDouble[i] << std::endl;
    }
}

// pass-by-reference
void double_v2(std::vector<int>& toDouble) {
    //
    for (int i = 0; i < toDouble.size(); i++) {
        toDouble[i] *= 2;
    }
}

//
int main() {
    //
    std::vector<int> xs = {0, 1, 2, 3};

    //
    double_v1(xs);

    //
    double_v2(xs);

    //
    std::cout << "main: xs:" << std::endl;
    for (int i = 0; i < xs.size(); i++) {
        std::cout << "  x = " << xs[i] << std::endl;
    }

    //
    return 0;
}
