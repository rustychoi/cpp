#!/usr/bin/env bash

( 
    set -x
    rm -rfv 'CMakeCache.txt' 'CMakeFiles/' 'Makefile' 'cmake_install.cmake' 'main'
)
